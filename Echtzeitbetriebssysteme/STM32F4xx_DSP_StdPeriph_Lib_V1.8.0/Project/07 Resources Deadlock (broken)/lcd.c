#include "stm32f4xx.h"
#include "lcd.h"
#include "main.h"

void lcdpins(void) {
	GPIO_InitTypeDef GPIO_IS;
	
	/* Enable the GPIOA peripheral */ 
  RCC_AHB1PeriphClockCmd(LCD_RCCGPIO, ENABLE);
  
  /* Configure LCD pins in output */
	GPIO_IS.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_IS.GPIO_OType = GPIO_OType_PP;
	GPIO_IS.GPIO_Pin = LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7 | LCD_E | LCD_RS | LCD_RW;
	GPIO_IS.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_IS.GPIO_Speed = GPIO_Speed_2MHz;
	
  GPIO_Init(LCD_GPIO, &GPIO_IS);
	
}


void pulse_e(void){
//erzeugt einen einzelnen Impuls auf
//der Enable (Strobe) Leitung
	
	Delay(1);
		GPIO_SetBits(LCD_GPIO, LCD_E); //steigene Flanke
	Delay(1);
		GPIO_ResetBits(LCD_GPIO, LCD_E); //fallende Flanke
	Delay(1);
	
}

void lcd_init(void){

//die LCD-Initialisierung ist etwas muehsam. Es sind insgesamt 9 Schritte zu setzen
//Legende: RS RW  -  D7 D6 D5 D4 (D3 D2 D1 D0)
	
Delay(15);
// 1. Schritt: Interface auf 8 Bit
GPIO_ResetBits(LCD_GPIO, LCD_RS | LCD_RW | LCD_D7 | LCD_D6);
GPIO_SetBits(LCD_GPIO, LCD_D5 | LCD_D4);
	pulse_e(); //00-0011....
	
  Delay(5);  //2. Schritt:	(wie vorher)
	pulse_e(); //00-0011.... (wie vorher)
	
  Delay(1);	 //3. Schritt:	(wie vorher)
	pulse_e(); //00-0011.... (wie vorher)

  //4. Schritt:	Interface auf 4 Bit
GPIO_ResetBits(LCD_GPIO, LCD_D4);
	pulse_e(); //00-0010
	
//ab jetzt wird jedes Byte in ZWEI HAELFTEN
//an das LC-Display uebertragen. Zuerst 7654, dann 3210.

//5. Schritt: 2zeilig, 5x8
	pulse_e(); //00-0010....
GPIO_ResetBits(LCD_GPIO, LCD_D5);
GPIO_SetBits(LCD_GPIO, LCD_D7);
	pulse_e(); //+....1000

//6. Schritt: Display aus
GPIO_ResetBits(LCD_GPIO, LCD_D7);
	pulse_e();  //00-0000....
GPIO_SetBits(LCD_GPIO, LCD_D7);
	pulse_e(); //+....1000

//7. Schritt: Display loeschen
GPIO_ResetBits(LCD_GPIO, LCD_D7);
	pulse_e();  //00-0000....
GPIO_SetBits(LCD_GPIO, LCD_D4);
	pulse_e(); //+....0001

//8. Schritt: Cursor rechts, kein Display shift
GPIO_ResetBits(LCD_GPIO, LCD_D4);
	pulse_e();  //00-0000....
GPIO_SetBits(LCD_GPIO, LCD_D6 | LCD_D5);
	pulse_e(); //+....0110

//9. Schritt
GPIO_ResetBits(LCD_GPIO, LCD_D6 | LCD_D5);
	pulse_e();  //00-0000....
GPIO_SetBits(LCD_GPIO, LCD_D7 | LCD_D6 );
	pulse_e();  //+....1100

}

void lcd_send_command(int command_value){
//Schickt ein einzelnes Byte als BEFEHL an das Display
//(aber natuerlich wie immer auf 2 Haelften aufgeteilt)
GPIO_ResetBits(LCD_GPIO, LCD_RS | LCD_RW);
GPIO_ResetBits(LCD_GPIO, LCD_D7 | LCD_D6 | LCD_D5 | LCD_D4); //zuerst alles auf null
if (command_value&(1<<7)) GPIO_SetBits(LCD_GPIO, LCD_D7);    //und dann selektiv
if (command_value&(1<<6)) GPIO_SetBits(LCD_GPIO, LCD_D6);    //die einzelnen
if (command_value&(1<<5)) GPIO_SetBits(LCD_GPIO, LCD_D5);    //Datenbits D7-D4 setzen
if (command_value&(1<<4)) GPIO_SetBits(LCD_GPIO, LCD_D4);    //oder eben nicht
	pulse_e(); //00-7654....
	
GPIO_ResetBits(LCD_GPIO, LCD_D7 | LCD_D6 | LCD_D5 | LCD_D4); //zuerst alles auf null
if (command_value&(1<<3)) GPIO_SetBits(LCD_GPIO, LCD_D7);    //und dann selektiv
if (command_value&(1<<2)) GPIO_SetBits(LCD_GPIO, LCD_D6);    //die einzelnen
if (command_value&(1<<1)) GPIO_SetBits(LCD_GPIO, LCD_D5);    //Datenbits D7-D4 setzen
if (command_value&(1<<0)) GPIO_SetBits(LCD_GPIO, LCD_D4);    //oder eben nicht
	pulse_e(); //+....3210
	
}

void lcd_send_data(int data_value){
//Schickt ein einzelnes Byte als DATEN an das Display
//(aber natuerlich wie immer auf 2 Haelften aufgeteilt)
GPIO_SetBits(LCD_GPIO, LCD_RS);	
GPIO_ResetBits(LCD_GPIO, LCD_RW);
GPIO_ResetBits(LCD_GPIO, LCD_D7 | LCD_D6 | LCD_D5 | LCD_D4); //zuerst alles auf null
if (data_value&(1<<7)) GPIO_SetBits(LCD_GPIO, LCD_D7);    //und dann selektiv
if (data_value&(1<<6)) GPIO_SetBits(LCD_GPIO, LCD_D6);    //die einzelnen
if (data_value&(1<<5)) GPIO_SetBits(LCD_GPIO, LCD_D5);    //Datenbits D7-D4 setzen
if (data_value&(1<<4)) GPIO_SetBits(LCD_GPIO, LCD_D4);    //oder eben nicht
	pulse_e(); //10-7654....
	
GPIO_ResetBits(LCD_GPIO, LCD_D7 | LCD_D6 | LCD_D5 | LCD_D4); //zuerst alles auf null
if (data_value&(1<<3)) GPIO_SetBits(LCD_GPIO, LCD_D7);    //und dann selektiv
if (data_value&(1<<2)) GPIO_SetBits(LCD_GPIO, LCD_D6);    //die einzelnen
if (data_value&(1<<1)) GPIO_SetBits(LCD_GPIO, LCD_D5);    //Datenbits D7-D4 setzen
if (data_value&(1<<0)) GPIO_SetBits(LCD_GPIO, LCD_D4);    //oder eben nicht
	pulse_e(); //+....3210
	
}

void lcd_cursorpos(int zeile, int spalte){
//setzt den Cursor an die angegebene Position
//Zeile ist 0...1 (fuer links/rechts)
//Spalte ist 0...7 (eh klar)
	
lcd_send_command(0x80 + zeile*0x40 + spalte);	

}
