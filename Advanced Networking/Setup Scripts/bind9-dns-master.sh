#!/bin/bash

apt-get install -y bind9 bind9utils bind9-doc dnsutils
mkdir -p /etc/bind/zones

# Basic dns setup with allowed hosts and forwarders
cat <<EOT > /etc/bind/named.conf.options
acl "clients" {
	127.0.0.0/8;
	10.0.5.0/24;
    192.168.0.0/16;
};

options {  
	directory "/var/cache/bind";  
	recursion yes;
	allow-recursion { clients; };
	allow-query { clients; };
	allow-transfer { localhost; 10.0.5.2; };
	listen-on { localhost; 10.0.5.1; };

	forwarders {
		1.1.1.1;
		8.8.4.4;
	};
};
EOT

# Configure available zones
cat <<EOT > /etc/bind/named.conf.local
zone "bbauer.ant.nwlab"  {
	type master;
	file "/etc/bind/zones/bbauer.ant.nwlab";
};

zone "5.0.10.in-addr.arpa"  {
	type master;
	file "/etc/bind/zones/rev.5.0.10.in-addr.arpa";
};
EOT

# Configure dns record for bbauer.ant.nwlab
cat <<EOT > /etc/bind/zones/bbauer.ant.nwlab
; BIND data file for bbauer.ant.nwlab
;
\$ORIGIN bbauer.ant.nwlab.
\$TTL 300
@ IN SOA ns1.bbauer.ant.nwlab. host.bbauer.ant.nwlab. (
201006601 ; Serial
7200 ; Refresh
120 ; Retry
2419200 ; Expire
604800) ; Default TTL
;
bbauer.ant.nwlab. IN NS ns1.bbauer.ant.nwlab.
bbauer.ant.nwlab. IN NS ns2.bbauer.ant.nwlab.
bbauer.ant.nwlab. IN MX 10 mail.bbauer.ant.nwlab.

bbauer.ant.nwlab. IN A 10.0.5.100
mail IN A 10.0.5.10
ns1 IN A 10.0.5.1
ns2 IN A 10.0.5.2
www IN CNAME bbauer.ant.nwlab.
EOT

# Configure reverse zone
cat <<EOT > /etc/bind/zones/rev.5.0.10.in-addr.arpa
\$ORIGIN 5.0.10.in-addr.arpa.
@ IN SOA bbauer.ant.nwlab. host.bbauer.ant.nwlab. (
2010081401;
28800;
604800;
604800;
86400 );

@ IN NS ns1.bbauer.ant.nwlab.
@ IN NS ns2.bbauer.ant.nwlab.
100 IN PTR bbauer.ant.nwlab.
EOT

# Set correct permissions
chown -R bind: /etc/bind/zones

# Finish by restarting bind and execute dig
named-checkconf /etc/bind/named.conf
/etc/init.d/bind9  restart
dig bbauer.ant.nwlab

echo "DONE"