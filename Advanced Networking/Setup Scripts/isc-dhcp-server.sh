#!/bin/bash

# Interface for which DHCP should be provided
INTERFACESv4=ens37


apt-get update
apt-get install -y isc-dhcp-server

sed -e 's/^#DHCPDv4_CONF=/DHCPDv4_CONF=/g' \
-e 's/^#DHCPDv4_PID=/DHCPDv4_PID=/g' \
-e "s/INTERFACESv4=\"\"/INTERFACESv4=\"${INTERFACESv4}\"/g" \
-i /etc/default/isc-dhcp-server

cat <<EOT > /etc/dhcp/dhcpd.conf
subnet 10.0.5.0 netmask 255.255.255.0 {
	option domain-name "bbauer.ant.nwlab";
	option domain-name-servers 10.0.5.1, 10.0.5.2;
	option routers 10.0.5.1;
	option subnet-mask 255.255.255.0;
	option broadcast-address 10.0.5.255;
	range 10.0.5.150 10.0.5.250;
}

host webserver {
	hardware ethernet 52:54:00:e9:ee:38;
	fixed-address 10.0.5.100;
}
EOT


# Enable autostart and start the dhcp server
systemctl enable isc-dhcp-server
systemctl restart isc-dhcp-server

echo "DONE"