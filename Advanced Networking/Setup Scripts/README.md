# General info for the install scripts

## IP Mapping

* 10.0.5.1 Gateway
* 10.0.5.2 Secondary DNS
* 10.0.5.10 Mailserver
* 10.0.5.100 Webserver
* 10.0.5.150-10.0.5.250 DHCP Assigned addresses (dynamic)